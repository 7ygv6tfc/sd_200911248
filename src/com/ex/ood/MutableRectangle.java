package com.ex.ood;

import com.ex.ood.Rectangle2;

public class MutableRectangle extends Rectangle {
	public void setWidth (int w) {
		width = w;
	}
	public void setHeight (int h) {
		height = h;
	}
}
