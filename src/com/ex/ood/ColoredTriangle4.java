package com.ex.ood;

import java.awt.Color;
import java.awt.Point;
import com.ex.ood.Triangle3;

public class ColoredTriangle4 extends Triangle3{
	private Color color;

	public ColoredTriangle4 (Color c, Point p1, Point p2, Point p3) {
		super(p1, p2, p3);
		
		if(c == null) c = Color.red;
		color = c;
	}

	public boolean equals(Object obj) {
		if(obj == null) return false;

		//if(!super.equals(obj)) return false;

		ColoredTriangle4 otherColoredTriangle = (ColoredTriangle4) obj;
	
	return super.equals(otherColoredTriangle) &&
		this.color.equals(otherColoredTriangle.color);
	}
}

	
