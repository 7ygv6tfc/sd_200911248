package com.ex.ood;

public class Sorter
{
	public static void sort(Object[] data, Ch3Comparator comp)
	{
	for (int i = data.length-1;i >=1;i--) {
	int indexOfMax = 0;
	for (int j = 1; j <=i;j++) {
	if (comp.compare(data[j], data[indexOfMax]) > 0)
	indexOfMax = j;
	}
	Object temp = data[i];
	data[i] = data[indexOfMax];
	data[indexOfMax] = temp;
	}

	for(int j = 0;j<data.length;j++) {
		System.out.println("Result = " + data[j]);
	}
}
}
