package com.ex.ood;

import java.awt.Color;
import java.awt.Point;
import com.ex.ood.Triangle1;

public class ColoredTriangle2 extends Triangle1{
	private Color color;

	public ColoredTriangle2 (Color c, Point p1, Point p2, Point p3) {
		super(p1, p2, p3);
		
		if(c == null) c = Color.red;
		color = c;
	}

	public boolean equals(Object obj) {
		if(obj instanceof ColoredTriangle2) {
			ColoredTriangle2 otherColoredTriangle = (ColoredTriangle2)obj;
			
			return super.equals(otherColoredTriangle) && 
				this.color.equals(otherColoredTriangle.color);
		}

		else if(obj instanceof Triangle1) {
			return super.equals(obj);
		}

		else
			return false;
		}
}
