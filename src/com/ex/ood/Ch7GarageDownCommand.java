package com.ex.ood;

public class Ch7GarageDownCommand implements Ch7Command {
	Ch7GarageDoor gDoor;

	public Ch7GarageDownCommand(Ch7GarageDoor garageDoor) {
		this.gDoor = garageDoor;
	}

	public void execute() {
		gDoor.up();
	}
}
