package com.ex.ood;

public class IntegerComparator implements Ch3Comparator {
	public int compare(Object o1, Object o2) {
		return (Integer) o1 - (Integer) o2;
	}
}
