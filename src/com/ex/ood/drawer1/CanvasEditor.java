package com.ex.ood.drawer1;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class CanvasEditor implements ActionListener, MouseListener{
	private JButton currentButton; 

	public CanvasEditor(JButton e) {
		currentButton = e;	
	}

	public void actionPerformed(ActionEvent e) {
		currentButton = (JButton) e.getSource();
	}	

	public void mouseClicked(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();

		JPanel canvas = (JPanel) e.getSource();
		
		if(currentButton.getText().equals("Ellipse"))
			canvas.getGraphics().drawOval(x-30, y-20, 60,40);

		else if(currentButton.getText().equals("Rect"))
			canvas.getGraphics().drawRect(x-30,y-20,60,40);

		else if(currentButton.getText().equals("Square"))
			canvas.getGraphics().drawRect(x-25,y-25,550,50);
	}

	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
}
