package com.ex.ood.drawer1;

import javax.swing.*;
import java.awt.*;

public class DrawingFrame extends JFrame {
	public DrawingFrame() {
		super("Drawing Applicaiton");
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		JComponent drawingCanvas = createDrawingCanvas();
		add(drawingCanvas, BorderLayout.CENTER);

		JToolBar toolbar = createToolbar(drawingCanvas);
		add(toolbar, BorderLayout.NORTH);
	}

	private JComponent createDrawingCanvas() {
		JComponent drawingCanvas = new JPanel();
		drawingCanvas.setPreferredSize(new Dimension(400,300));
		drawingCanvas.setBackground(Color.white);
		drawingCanvas.setBorder(BorderFactory.createEtchedBorder());
		
		return drawingCanvas;
	}

	private JToolBar createToolbar(JComponent canvas) {
		JToolBar toolbar = new JToolBar();
		JButton ellipseButton = new JButton("Ellipse");
		toolbar.add(ellipseButton);
		JButton squareButton = new JButton("Square");
		toolbar.add(squareButton);
		JButton rectButton = new JButton("Rect");
		toolbar.add(rectButton);
		
		CanvasEditor canvasEditor = new CanvasEditor(ellipseButton);
		ellipseButton.addActionListener(canvasEditor);
		squareButton.addActionListener(canvasEditor);
		rectButton.addActionListener(canvasEditor);
		canvas.addMouseListener(canvasEditor);

		return toolbar; 
	}

}
