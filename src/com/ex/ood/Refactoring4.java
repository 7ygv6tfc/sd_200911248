package com.ex.ood;

public class Refactoring4{
	public double getTotalBill() {
		double roomCharge = getroomCharge();
		double mealCharge = getmealCharge();
		double movieCharge = getmovieCharge();

		return roomCharge + mealCharge + movieCharge ;
	}

	public double getroomCharge() {
		return 50000;
	}

	public double getmealCharge() {
		return 12000;
	}

	public double getmovieCharge() {
		return 18000;
	}
}
