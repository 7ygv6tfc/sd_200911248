package com.ex.ood.candy;

public class NoQuarterState implements State {
	GumballMachine machine;

	public NoQuarterState(GumballMachine machine) {
		this.machine = machine;
	}

	public void insertQuarter() {
		System.out.println("동전을 넣으셨습니다.");
		machine.setState(machine.getHasQuarterState());
	}

	public void ejectQuarter() {
		System.out.println("동전을 넣어주세요.");
	}

	public void turnCrank() {
		System.out.println("동전을 넣어주세요.");
	}

	public void dispense() {
		System.out.println("동전을 넣어주세요.");
	}
}
