package com.ex.ood.candy;

public class SoldState implements State {
	GumballMachine machine;

	public SoldState(GumballMachine machine) {
		this.machine = machine;
	}

	public void insertQuarter() {
		System.out.println("알맹이가 나가고 있습니다.");
	}

	public void ejectQuarter() {
		System.out.println("이미 뽑았습니다");
	}

	public void turnCrank() {
		System.out.println("손잡이를 한번만 돌려주세요.");
	}

	public void dispense() {
		machine.releaseBall();
		if(machine.getCount() > 0) {
			machine.setState(machine.getNoQuarterState());
		} else {
			System.out.println("알맹이가 없습니다.");
			machine.setState(machine.getSoldOutState());
		}
	}
	
}
