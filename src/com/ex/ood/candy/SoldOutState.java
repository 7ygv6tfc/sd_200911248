package com.ex.ood.candy;

public class SoldOutState implements State {
	GumballMachine machine;

	public SoldOutState(GumballMachine machine) {
		this.machine = machine;
	}

	public void insertQuarter() {
		System.out.println("동전을 넣으셨습니다. ");
	}

	public void ejectQuarter() {
		System.out.println("상품이 없습니다.");
	}

	public void turnCrank() {
		System.out.println("상품이 없습니다.");
	}

	public void dispense() {
		System.out.println("상품이 없습니다.");
	}
}
