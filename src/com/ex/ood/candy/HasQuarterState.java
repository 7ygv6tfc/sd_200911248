package com.ex.ood.candy;

public class HasQuarterState implements State {
	GumballMachine machine;

	public HasQuarterState(GumballMachine machine) {
		this.machine = machine;
	}

	public void insertQuarter() {
	System.out.println("동전 한개만 넣어주세요.");
	}

	public void ejectQuarter() {
		System.out.println("동전이 반환됩니다.");
		machine.setState(machine.getNoQuarterState());
	}

	public void turnCrank() {
		System.out.println("손잡이를 돌렸습니다.");
	}

	public void dispense() {
		System.out.println("알맹이가 나갈 수 없습니다.");
	}
}
