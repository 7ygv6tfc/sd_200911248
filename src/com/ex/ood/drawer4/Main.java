
package com.ex.ood.drawer4;

import com.ex.ood.drawer4.figure.Figure;

public class Main {
    public static void main(String[] args)     {
        DrawingFrame drawFrame = new DrawingFrame();
        drawFrame.pack();
        drawFrame.setVisible(true);
    }
}

