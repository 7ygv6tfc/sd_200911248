package com.ex.ood;

import com.ex.ood.Person2;

public class Employee2 {
	private Person2 me2;
	private float salary;
	private String name;

	public Employee2(Person2 p2, float salary) {
	me2 = p2;
	this.salary = salary;
	}

	public String getName() {
	this.name = me2.getName();
	return name;
	}

	public float getSalary() {
	return salary;
	}
}

