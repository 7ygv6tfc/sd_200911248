package com.ex.ood.drawer3;

import java.awt.*;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class Drawing implements Iterable<Figure> {
	private List<Figure> figures;

	public Drawing() {
		figures = new ArrayList<Figure>();
	}

	public void addFigure(Figure newFigure) {
		figures.add(newFigure);
	}

	public Iterator<Figure> iterator() {
		return figures.iterator();
	}
}
