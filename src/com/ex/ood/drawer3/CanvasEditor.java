package com.ex.ood.drawer3;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;


public class CanvasEditor implements MouseListener  {
	
	private Figure currentFigure;

	public CanvasEditor(Figure figure) {
		this.currentFigure = figure;
	}

	public void setCurrentFigure(Figure newFigure) {
		currentFigure = newFigure;
	}
	
	public void mouseClicked(MouseEvent e) {
		Figure newFigure = (Figure) currentFigure.clone();
		newFigure.setCenter(e.getX(), e.getY());
		((DrawingCanvas)e.getSource()).addFigure(newFigure);
	}

	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
}

	
