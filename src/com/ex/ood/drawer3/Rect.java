package com.ex.ood.drawer3;

import java.awt.*;

public class Rect extends Figure {
	public Rect(int centerX, int centerY, int w, int h) {
		super(centerX,centerY,w,h);
	}

	public void draw(Graphics g) {
		int width = getWidth();
		int height = getHeight();

		g.drawRect(getCenterX() - width /2, getCenterY() - height /2, width, height);
	}
}
