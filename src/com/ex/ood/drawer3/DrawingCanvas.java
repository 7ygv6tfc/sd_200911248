package com.ex.ood.drawer3;

import java.awt.*;
import javax.swing.*;

public class DrawingCanvas extends JPanel {
	private Drawing drawing;

	public DrawingCanvas() {
		this.drawing = new Drawing();
		setBackground(Color.white);
		setPreferredSize(new Dimension(400,300));
		setBorder(BorderFactory.createEtchedBorder());
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for(Figure figure : drawing) {
			figure.draw(g);
		}
	}

	public void addFigure(Figure newFigure) {
		drawing.addFigure(newFigure);
		repaint();
	}
}
