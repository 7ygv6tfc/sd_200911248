package com.ex.ood;



import java.awt.*;

import java.awt.Rectangle;

import java.util.ArrayList;

import java.util.List;

import java.util.LinkedList;

import java.util.Iterator;

import java.awt.Color;

import java.awt.Point;

public class sd_2_200911248

{

	public static void main(String[] args)

	{

		//ch2main();

		//ch3main();

		//ch4main();

		//ch5main();

		ch7main();

	}

	public static void ch7main() {

	//Factory Pattern
	String pizza = "cheese";
	System.out.println("ch7-Factory pattern");

	Ch7SimplePizzaFactory factory = new Ch7SimplePizzaFactory();
	factory.createPizza(pizza);

	Ch7PizzaStore store = new Ch7PizzaStore(factory);
	store.orderPizza(pizza);
	System.out.println("\n");

	//Command Pattern
	System.out.println("ch7-Command pattern");
	Ch7SimpleRemoteControl remote = new Ch7SimpleRemoteControl();

	Ch7Light light = new Ch7Light();
	Ch7LightOnCommand lighton = new Ch7LightOnCommand(light);
	Ch7LightOffCommand lightoff = new Ch7LightOffCommand(light);

	remote.setCommand(lighton);
	remote.buttonWasPressed();

	remote.setCommand(lightoff);
	remote.buttonWasPressed();

	Ch7GarageDoor garage = new Ch7GarageDoor();
	Ch7GarageDoorOpenCommand garageUp = new Ch7GarageDoorOpenCommand(garage);
	Ch7GarageDownCommand garageDown = new Ch7GarageDownCommand(garage);

	remote.setCommand(garageUp);
	remote.buttonWasPressed();

	remote.setCommand(garageDown);
	remote.buttonWasPressed();
	System.out.println("\n");

	//Singleton Pattern
	System.out.println("ch7-Singleton pattern");
	Ch7SingPatternLogger singleton = Ch7SingPatternLogger.getInstance();
	singleton.readEntireLog();
	System.out.println("\n");

	//Duck
	System.out.println("ch7-Duck");
	Ch7MallardDuck duck = new Ch7MallardDuck();
	duck.quack();
	duck.fly();

	Ch7WildTurkey turkey = new Ch7WildTurkey();
	turkey.gobble();
	turkey.fly();

	Ch7TurkeyAdapter turkeyadapter = new Ch7TurkeyAdapter(turkey);
	turkeyadapter.quack();
	turkeyadapter.fly();
	}

	
	public static void ch5main() {

	Ch5ObsPatternView view = new Ch5ObsPatternView();
	Ch5ObsPatternModel model = new Ch5ObsPatternModel();

	model.addObserver(view);
	model.changeSomething();
	


	int x=2;
	int y=5;

	Ch5FixedPointv1New v1 = new Ch5FixedPointv1New(x,y);
	System.out.println("실행전 : x → " + v1.getX() + " , y → " + v1.getY());
	System.out.println("\n");
	
	Ch5FixedPointv2Inheritance v2 = new Ch5FixedPointv2Inheritance(x,y);
	System.out.println("v2 실행전 : x → " + v2.getX() + " , y → " + v2.getY());
	v2.setLocation(5,2);
	System.out.println("v2 실행후 : x → " + v2.getX() + " , y → " + v2.getY());
	System.out.println("\n");

	Ch5FixedPointv3Association v3 = new Ch5FixedPointv3Association(new Point(2,5));
	System.out.println("v3 실행전 : x → " + v3.getX() + " , y → " + v3.getY());

	System.out.println("v3 실행후 : x → " + v3.getX() + " , y → " + v3.getY());
	System.out.println("\n");
}

	public static void ch4main() {

	System.out.println("--ch4-Refactoring");
	Refactoring1 ref1 = new Refactoring1();
	Refactoring2 ref2 = new Refactoring2();
	Refactoring3 ref3 = new Refactoring3();
	Refactoring4 ref4 = new Refactoring4();

	System.out.println("getRoomCharge() =" + ref1.getRoomCharge() );
	System.out.println("getTotalBill() =" + ref2.getTotalBill() );
	System.out.println("explaining variable =" + ref3.getTotalBill() );
	System.out.println("temp with query1 =" + ref4.getTotalBill() );
	System.out.println("");


	System.out.println("--ch4-Triangle");



	Triangle1 t = new Triangle1(new Point(0,0), new Point(1,1), new Point(2,2));



	ColoredTriangle1 ct = new ColoredTriangle1(Color.red, new Point(0,0), new Point(1,1), new Point(2,2));



	System.out.println("Case 1");

	System.out.println(t.equals(ct));

	System.out.println(ct.equals(t));



	Triangle2 t2 = new Triangle2(new Point(0,0), new Point(1,1), new Point(2,2));



	ColoredTriangle2 rct = new ColoredTriangle2(Color.red, new Point(0,0), new Point(1,1), new Point(2,2));

	ColoredTriangle2 bct = new ColoredTriangle2(Color.blue, new Point(0,0), new Point(1,1), new Point(2,2));



	System.out.println("Case 2");

	System.out.println(rct.equals(t));

	System.out.println(t.equals(bct));

	System.out.println(rct.equals(bct));

	

	ColoredTriangle3 rct2 = new ColoredTriangle3(Color.red, new Point(0,0), new Point(1,1), new Point(2,2));

	ColoredTriangle3 bct2 = new ColoredTriangle3(Color.blue, new Point(0,0), new Point(1,1), new Point(2,2));

	

	System.out.println("Case 3");

	System.out.println(rct2.equals(t2));

	System.out.println(t2.equals(bct2));

	System.out.println(rct2.equals(bct2));



	Triangle3 t3 = new Triangle3(new Point(0,0), new Point(1,1), new Point(2,2));



	ColoredTriangle4 rct3 = new ColoredTriangle4(Color.red, new Point(0,0), new Point(1,1), new Point(2,2));

	ColoredTriangle4 bct3 = new ColoredTriangle4(Color.blue, new Point(0,0), new Point(1,1), new Point(2,2));



	System.out.println("Case 4");

	System.out.println("*reflexive :  Triangle.equals(Triangle)  ->" + t3.equals(t3) + "\n" );

	System.out.println("*symmetric :  Triangle.equals(rct3)      ->" + t3.equals(rct3) );

	System.out.println("              rct3.equals(Triangle)      ->" + t3.equals(rct3) + "\n"  );

	System.out.println("*transitive : Triangle.equals(rct3)      ->" + t3.equals(rct3) );

	System.out.println("*             rct3.equals(bct3)          ->" + rct3.equals(bct3) );

	System.out.println("*             Triangle.equals(bct3)      ->" + t3.equals(bct3) + "\n" );

	System.out.println("*consistent : Triangle.equals(Triangle)  ->" + t3.equals(t3)  + "\n" );

	System.out.println("*non_null :   Triangle.equals(null)      ->" + t3.equals(null) );

}





	public static void ch3main()

	{

		System.out.println("--ch3-sorter");



		String[] B = {"Starbucks", "Nescafe", "Hollys", "TomTom"};

		Ch3Comparator stringComp = new Ch3StringComparator();

		Sorter.sort(B, stringComp);



		Integer[] C = {new Integer(3), new Integer(1), new Integer(4), new Integer(2)};

		Ch3Comparator IntegerComp = new IntegerComparator();

		Sorter.sort(C, IntegerComp);



		System.out.println("--ch3-person");

		Person2 me = new Person2("Lee");

		Student2 st = new Student2(me, (float) 4.5);

		System.out.println("Name =" + st.getName() + "\n GPA = " + st.getGPA());



		Person2 me2 = new Person2("SangHun");

		Employee2 ey = new Employee2(me2, (float) 1200);

		System.out.println("Name =" + ey.getName() + "\n Salary = " + ey.getSalary());

	}

	public static void ch2main()

	{

		System.out.println("--ch2-Rectangle");

		EnhancedRectangle rectangle = new EnhancedRectangle(1, 2, 50, 60);

		rectangle.setLocation(10, 10);

		rectangle.setCenter(50, 60);



		System.out.println("--ch2-List");

		

		List<String> arrayList = new ArrayList<String>();



		arrayList.add("A");

		arrayList.add("B");

		arrayList.add("C");

		arrayList.add("D");

		

		Iterator<String> arrayItr = arrayList.iterator();

		while(arrayItr.hasNext()) {

			String string = (String)arrayItr.next();

			System.out.println(string);

		}



		arrayList.clear();

		System.out.println("After Clear \n");



		Iterator<String> arrayItr2 = arrayList.iterator();

		while(arrayItr2.hasNext()) {

		String string2 = (String)arrayItr2.next();

		System.out.println(string2 + "\n");

		}





		List<String> linkedList = new LinkedList<String>();



		linkedList.add("1");

		linkedList.add("2");

		linkedList.add("3");

		linkedList.add("4");

		

		Iterator<String> linkedItr = linkedList.iterator();

		while(linkedItr.hasNext()) {

			String string = (String)linkedItr.next();

			System.out.println(string);

		}



		linkedList.clear();

		System.out.println("After Clear \n");



		Iterator<String> linkedItr2 = linkedList.iterator();

		while(linkedItr2.hasNext()) {

		String string2 = (String)linkedItr2.next();

		System.out.println(string2 + "\n");

		}





		



		System.out.println("--ch2-Automobile");

		Automobile[] fleet = new Automobile[3];



		fleet[0] = new Sedan();

		fleet[1] = new Minivan();

		fleet[2] = new SportsCar();

		

		int totalCapacity = 0;

		for(int i =0;i<fleet.length;i++)

		{

		totalCapacity += fleet[i].getCapacity();

		}

		System.out.println("totalCapacity = " + totalCapacity);		



	}

}
